<?php
$this->load->view('template/head');
?>

<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Daftar Pengajuan Layanan</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>NIM</th>
                            <!-- <th>Nama</th> -->
                            <th>Keperluan</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $r = 0;
                        foreach ($semuadata

                        as $data){
                        $r++; ?>
                        <tr>
                            <td><?php echo $r; ?></td>
                            <td><?php echo $data['created_at']; ?></td>
                            <td><?php echo $data['nim']; ?></td>
                            <!-- <td><?php echo $data['nama']; ?></td> -->
                            <td><?php echo $data['pmhsaktif_keperluan']; ?></td>
                            <td align="center">
                                <a href="<?php echo 'aktifkuliah/cetak/'.$data['pmhsaktif_id'] ?>" target="blank"><button type="button" class="btn btn-success">Cetak</button></a>

                                <span onclick="validasi_ttd(this)" id="<?php echo $data['pmhsaktif_id']; ?>" value=''>
                  
                
                <?php if ($data['pmhsaktif_ttd'] == 1) {
                    echo '<input type="checkbox" checked data-toggle="toggle" data-on="Sudah Tandatangan" data-off="Belum Tandatangan" data-onstyle="success" data-offstyle="danger" data-size="normal"></td>';
                } else {
                    echo '<input type="checkbox" data-toggle="toggle" data-on="Sudah Tandatangan" data-off="Belum Tandatangan" data-onstyle="success" data-offstyle="danger" data-size="normal"></td>';
                }                } ?>

                        </tbody>
                        
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>


</section>


<?php
$this->load->view('template/js');
?>

<!--tambahkan custom js disini-->

<!-- DataTables -->
<script src="<?php echo base_url('assets/custom/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/custom/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
    function validasi_ttd(params) {
        // alert('wowow');
        // console.log('wwoowo');
        url = "<?php echo base_url('aktifkuliah/update_validasi/') ?>" + '/' + params.id;
        console.log(url);
        $.ajax({
            url: url,
            type: "Get",
            success: function (res) {

            }
        });

    }
</script>

<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script>
<!-- http://www.bootstraptoggle.com/ -->

<?php
$this->load->view('template/foot');
?>
