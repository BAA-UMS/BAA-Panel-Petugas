<?php
$this->load->view('template/head');
?>

<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Daftar Pengajuan Layanan</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>NIM</th>
                            <th>Keperluan</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($semuadata

                        as $index => $value){
                        ?>
                        <tr>
                            <td><?php echo $index + 1; ?></td>
                            <td><?php echo $value['created_at']; ?></td>
                            <td><?php echo $value['nim']; ?></td>
                            <td><?php echo $value['kategori_nama']; ?></td>
                            <td align="center">
                <span onclick="validasi_bak(this)" id="<?php echo $value['pmhsstatus_id'] ?>" value=''>
                  
                
                <?php if ($value['pmhsstatus_bak'] == 1) {
                    echo '<input type="checkbox" checked data-toggle="toggle" data-on="Validasi" data-off="Belum Validasi" data-onstyle="success" data-offstyle="danger" data-size="normal"></td>';
                } else {
                    echo '<input type="checkbox" data-toggle="toggle" data-on="Validasi" data-off="Belum Validasi" data-onstyle="success" data-offstyle="danger" data-size="normal"></td>';
                }                } ?>

                </tr>;
              </span>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>


</section>


<?php
$this->load->view('template/js');
?>

<!--tambahkan custom js disini-->

<!-- DataTables -->
<script src="<?php echo base_url('assets/custom/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/custom/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>

<script type="text/javascript">
    document.title = 'hahaha';
    function validasi_bak(params) {
        // alert('wowow');
        // console.log('wwoowo');
        url = "<?php echo base_url('keuangan/update_validasi').'/' ?>" + params.id;
        console.log(url);
        $.ajax({
            url: url,
            type: "Get",
            success: function (res) {

            }
        });

    }
</script>

<!-- http://www.bootstraptoggle.com/ -->
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script>
    // setTimeout(function(){

    // })
    $(document).ready(function () {        
        $('#example1').DataTable({
            "pageLength": 20,
        });
        // $('#example2').DataTable({
        //   'paging'      : true,
        //   'lengthChange': false,
        //   'searching'   : false,
        //   'ordering'    : true,
        //   'info'        : true,
        //   'autoWidth'   : false,
        //   "pageLength": 50
        // })
    })
</script>
<?php
$this->load->view('template/foot');
?>
