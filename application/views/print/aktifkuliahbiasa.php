<!DOCTYPE html>
<!-- Created by pdf2htmlEX (https://github.com/coolwanglu/pdf2htmlex) -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/surataktifkuliah/aktifkuliahbiasa/base.min.css') ?>"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/surataktifkuliah/aktifkuliahbiasa/fancy.min.css') ?>"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/surataktifkuliah/aktifkuliahbiasa/main.css') ?>"/>
    <script src="<?php echo base_url('assets/js/surataktifkuliah/aktifkuliahbiasa/compatibility.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/surataktifkuliah/aktifkuliahbiasa/theViewer.min.js') ?>"></script>
    <script>
        try {
            theViewer.defaultViewer = new theViewer.Viewer({});
        } catch (e) {
        }
    </script>
    <script>
        window.print();
    </script>
    <title>SK Aktif</title>
</head>
<body>
<div id="sidebar">
    <div id="outline">
    </div>
</div>
<div id="page-container">
    <div id="pf1" class="pf w0 h0" data-page-no="1"><div class="pc pc1 w0 h0"><img class="bi x0 y0 w1 h1" alt="" src="<?php echo base_url('assets/images/surataktifkuliah/aktifkuliahbiasa/') ?>bg1.png"/><div class="c x1 y1 w0 h2"><div class="t m0 x2 h3 y2 ff1 fs0 fc0 sc0 ls0 ws0">UNIVERSITAS MUHAMMADIYAH SURAKARTA</div><div class="t m0 x3 h3 y3 ff1 fs0 fc0 sc0 ls0 ws0">BIRO ADMINISTRASI AKADEMIK</div><div class="t m0 x4 h4 y4 ff2 fs1 fc0 sc0 ls0 ws0">           JL<span class="fc1">. </span>A<span class="fc2">. </span>Yan<span class="fc3">i </span>Tromol Pos 1 P<span class="fc3">a</span>b<span class="fc3">e</span>lan Kartasura Telp<span class="fc2">. </span>(0271) 717417,719483 ext 106<span class="fc3"> </span>Fax<span class="fc3">. </span>(0271) 715448 </div><div class="t m0 x5 h4 y5 ff2 fs1 fc0 sc0 ls0 ws0">Su<span class="fc3">r</span>aka<span class="fc3">r</span>ta - 57102<span class="fc3">, <span class="fc4">http://www.ums.ac.id,</span></span> Email<span class="fc3">: </span>ums<span class="fc5">@</span>ums.ac<span class="fc1">.</span>id</div></div><div class="t m0 x6 h5 y6 ff1 fs2 fc1 sc0 ls0 ws0">SURAT KETERANGAN</div><div class="t m0 x7 h6 y7 ff2 fs2 fc0 sc0 ls0 ws0">xxx</div><div class="t m0 x0 h7 y8 ff3 fs2 fc1 sc0 ls0 ws0">Assalamu’alaikum Warohmatullahi Wabarakatuhu</div><div class="t m0 x0 h6 y9 ff2 fs2 fc1 sc0 ls0 ws0">Yang bertandatangan <span class="_ _0"></span>dibawah ini <span class="_ _0"></span>Pimpinan Universitas <span class="_ _0"></span>Muhammadiyah Surakarta, <span class="_ _0"></span>menerangkan </div><div class="t m0 x0 h6 ya ff2 fs2 fc1 sc0 ls0 ws0">bahwa :</div><div class="t m0 x8 h6 yb ff2 fs2 fc1 sc0 ls0 ws0">Nama<span class="_ _1"> </span>: XXXXXXXXXXXXX</div><div class="t m0 x8 h6 yc ff2 fs2 fc1 sc0 ls0 ws0">NIM<span class="_ _2"> </span>: <?php echo $datamhs['nim'] ?></div><div class="t m0 x8 h6 yd ff2 fs2 fc1 sc0 ls0 ws0">Program Studi<span class="_ _3"> </span>: XXXXXXXXXXXXX</div><div class="t m0 x8 h6 ye ff2 fs2 fc1 sc0 ls0 ws0">Jenjang<span class="_ _4"> </span>: XX</div><div class="t m0 x8 h6 yf ff2 fs2 fc1 sc0 ls0 ws0">Semester, Tahun Akademik<span class="_ _5"> </span>: XXXXXXXXXX</div><div class="t m0 x0 h6 y10 ff2 fs2 fc1 sc0 ls0 ws0">Adalah  benar-benar <span class="_ _6"> </span>Mahasiswa  Universitas <span class="_ _6"> </span>Muhammadiyah  Surakarta. <span class="_ _6"> </span>Surat  Keterangan <span class="_ _6"> </span>ini </div><div class="t m0 x0 h6 y11 ff2 fs2 fc1 sc0 ls0 ws0">diberikan kepada yang bersangkutan untuk keperluan : </div><div class="t m0 x0 h6 y12 ff2 fs2 fc1 sc0 ls0 ws0"><b><?php echo $datamhs['pmhsaktif_keperluan'] ?></b></div><div class="t m0 x0 h6 y13 ff2 fs2 fc1 sc0 ls0 ws0">Demikian surat keterangan ini dibuat, untuk dapat dipergunakan sebagaimana mestinya.</div><div class="t m0 x0 h7 y14 ff3 fs2 fc1 sc0 ls0 ws0">Wassalamu&apos;alaikum Warahmatullahi Wabarakatuhu </div><div class="t m0 x9 h6 y15 ff2 fs2 fc1 sc0 ls0 ws0">Surakarta, <?php echo $datamhs['created_at'] ?></div><div class="t m0 x9 h6 y16 ff2 fs2 fc1 sc0 ls0 ws0">an. Kepala BAA,</div><div class="t m0 x9 h6 y17 ff2 fs2 fc1 sc0 ls0 ws0">Kabag Pengajaran</div><div class="t m0 x9 h5 y18 ff1 fs2 fc1 sc0 ls0 ws0">Drs. Agus Mulyanto</div><div class="t m0 x9 h6 y19 ff2 fs2 fc1 sc0 ls0 ws0">NIK : 776</div><a class="l" href="http://www.ums.ac.id,"><div class="d m1" style="border-style:none;position:absolute;left:275.927002px;bottom:696.205994px;width:84.437988px;height:10.924011px;background-color:rgba(255,255,255,0.000001);"></div></a><a class="l" href="mailto:ums@ums.ac.id"><div class="d m1" style="border-style:none;position:absolute;left:390.442017px;bottom:696.205994px;width:60.994019px;height:10.924011px;background-color:rgba(255,255,255,0.000001);"></div></a></div><div class="pi" data-data='{"ctm":[1.000000,0.000000,0.000000,1.000000,0.000000,0.000000]}'></div></div>
</div>
<div class="loading-indicator">

</div>
</body>
</html>
