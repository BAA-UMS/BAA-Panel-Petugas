<!DOCTYPE html>
<!-- Created by pdf2htmlEX (https://github.com/coolwanglu/pdf2htmlex) -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link rel="stylesheet"
          href="<?php echo base_url('assets/css/suratperubahanstatus/suratpindahprogram/base.min.css') ?>"/>
    <link rel="stylesheet"
          href="<?php echo base_url('assets/css/suratperubahanstatus/suratpindahprogram/fancy.min.css') ?>"/>
    <link rel="stylesheet"
          href="<?php echo base_url('assets/css/suratperubahanstatus/suratpindahprogram/main.css') ?>"/>
    <script src="<?php echo base_url('assets/js/suratperubahanstatus/suratpindahprogram/compatibility.min.js') ?>"></script>
    <script src="<?php echo base_url('assets/js/suratperubahanstatus/suratpindahprogram/theViewer.min.js') ?>"></script>
    <script>
        try {
            theViewer.defaultViewer = new theViewer.Viewer({});
        } catch (e) {
        }
    </script>
    <!-- <script>
    window.print();
    </script> -->
    <title></title>
</head>
<body>
<div id="sidebar">
    <div id="outline">
    </div>
</div>
<div id="page-container">
    <div id="pf1" class="pf w0 h0" data-page-no="1"><div class="pc pc1 w0 h0"><img class="bi x0 y0 w1 h1" alt="" src="<?php echo base_url('assets/images/suratperubahanstatus/suratpindahprogram/') ?>bg1.png"/><div class="c x1 y1 w0 h2"><div class="t m0 x2 h3 y2 ff1 fs0 fc0 sc0 ls0 ws0">UNIVERSITAS MUHAMMADIYAH SURAKARTA</div><div class="t m0 x3 h4 y3 ff2 fs1 fc0 sc0 ls0 ws0">           JL<span class="fc1">. </span>A<span class="fc2">. </span>Yan<span class="fc3">i </span>Tromol Pos I P<span class="fc3">a</span>b<span class="fc3">e</span>lan Kartasura Telp<span class="fc2">. </span>(0271) 719483 (Hunting)<span class="fc3">, </span>Fax<span class="fc3">. </span>(0271) 715448 </div><div class="t m0 x4 h4 y4 ff2 fs1 fc0 sc0 ls0 ws0">Su<span class="fc3">r</span>aka<span class="fc3">r</span>ta - 57102<span class="fc3">, <span class="fc1">http://www.ums.ac.id/<span class="fc4">,</span></span></span> Email<span class="fc3">: </span>ums<span class="fc5">@</span>ums.ac<span class="fc1">.</span>id</div></div><div class="t m0 x5 h5 y5 ff2 fs2 fc1 sc0 ls0 ws0">SURAT KETERANGAN PINDAH PROGRAM</div><div class="t m0 x6 h5 y6 ff2 fs2 fc0 sc0 ls0 ws0">Nomor : XXXXXXXXX</div><div class="t m0 x7 h6 y7 ff3 fs2 fc1 sc0 ls0 ws0">Assalamu’alaikum Warohmatullahi Wabarakatuhu</div><div class="t m0 x7 h5 y8 ff2 fs2 fc1 sc0 ls0 ws0">Yang bertandatangan dibawah ini menerangkan bahwa mahasiswa atas nama berikut :</div><div class="t m0 x7 h5 y9 ff2 fs2 fc1 sc0 ls0 ws0">Nama<span class="_ _0"> </span>: XXXXXXXXXXXXX</div><div class="t m0 x7 h5 ya ff2 fs2 fc1 sc0 ls0 ws0">NIM<span class="_ _1"> </span>: <?php echo $datamhs['nim'] ?></div><div class="t m0 x7 h5 yb ff2 fs2 fc1 sc0 ls0 ws0">Program Studi<span class="_ _2"> </span>: XXXXXXXXXXXXX</div><div class="t m0 x7 h5 yc ff2 fs2 fc1 sc0 ls0 ws0">Fakultas<span class="_ _3"> </span>: XXXXXXXXXXXXX</div><div class="t m0 x7 h5 yd ff2 fs2 fc1 sc0 ls0 ws0">Semester Tahun Akademik<span class="_ _4"> </span>: XXXXXXXXXX</div><div class="t m0 x7 h5 ye ff2 fs2 fc1 sc0 ls0 ws0">Menyatakan pindah dari Program XXXXXXXX ke YYYYYYYYYY</div><div class="t m0 x7 h5 yf ff2 fs2 fc1 sc0 ls0 ws0">Segala <span class="_ _5"> </span>hal <span class="_ _5"> </span>terkait <span class="_ _5"> </span>administrasi <span class="_ _5"> </span>akademik <span class="_ _5"> </span>mahasiswa <span class="_ _5"> </span>besangkutan <span class="_ _5"> </span>selanjutnya <span class="_ _5"> </span>mengikuti </div><div class="t m0 x7 h5 y10 ff2 fs2 fc1 sc0 ls0 ws0">ketentuan yang berlaku pada</div><div class="t m0 x7 h5 y11 ff2 fs2 fc1 sc0 ls0 ws0"> </div><div class="t m0 x7 h5 y12 ff2 fs2 fc1 sc0 ls0 ws0">Program Studi<span class="_ _2"> </span>: YYYYYYY</div><div class="t m0 x7 h5 y13 ff2 fs2 fc1 sc0 ls0 ws0">NIM (tetap)<span class="_ _6"> </span>: </div><div class="t m0 x7 h5 y14 ff2 fs2 fc1 sc0 ls0 ws0">Efektif mulai Semester<span class="_ _7"> </span>: XXXXXXXXXX</div><div class="t m0 x7 h5 y15 ff2 fs2 fc1 sc0 ls0 ws0">Adapun <span class="_ _8"></span>administrasi <span class="_ _8"></span>keuangan <span class="_ _8"></span>mahasiswa <span class="_ _8"></span>mengikuti <span class="_ _8"></span>ketentuan <span class="_ _8"></span>ketentuan <span class="_ _8"></span>yang <span class="_ _8"></span>berlaku <span class="_ _8"></span>pada </div><div class="t m0 x7 h5 y16 ff2 fs2 fc1 sc0 ls0 ws0">Program Studi YYYYYYYYY cicilan I Semester XXXXXXXX.</div><div class="t m0 x7 h5 y17 ff2 fs2 fc1 sc0 ls0 ws0">Demikian surat keterangan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.</div><div class="t m0 x7 h6 y18 ff3 fs2 fc1 sc0 ls0 ws0">Wassalamu&apos;alaikum Warahmatullahi Wabarakatuhu </div><div class="t m0 x8 h5 y19 ff2 fs2 fc1 sc0 ls0 ws0">Surakarta, <?php echo $datamhs['created_at'] ?></div><div class="t m0 x8 h5 y1a ff2 fs2 fc1 sc0 ls0 ws0">a/n Plt. Kepala BAA,</div><div class="t m0 x8 h5 y1b ff2 fs2 fc1 sc0 ls0 ws0">SURYADI, SE</div><div class="t m0 x8 h5 y1c ff2 fs2 fc1 sc0 ls0 ws0">NIK: 315</div><div class="t m0 x7 h5 y1d ff2 fs2 fc1 sc0 ls0 ws0">Tembusan :</div><div class="t m0 x9 h5 y1e ff2 fs2 fc1 sc0 ls0 ws0">-<span class="_ _9"> </span>Yth. Ketua XXXXXXXXX</div><div class="t m0 x9 h5 y1f ff2 fs2 fc1 sc0 ls0 ws0">-<span class="_ _9"> </span>Yth. Ketua Unit Informasi Technology (IT).</div><div class="t m0 x9 h5 y20 ff2 fs2 fc1 sc0 ls0 ws0">-<span class="_ _9"> </span>Yth. Kepala Bagian Pengajaran BAA.</div><div class="t m0 x9 h5 y21 ff2 fs2 fc1 sc0 ls0 ws0">-<span class="_ _9"> </span>Yth. Kepala Urusan Kuitansi Keuangan.</div><a class="l" href="Http://www.ums.ac.id,"><div class="d m1" style="border-style:none;position:absolute;left:266.257996px;bottom:765.078979px;width:87.077026px;height:10.924011px;background-color:rgba(255,255,255,0.000001);"></div></a><a class="l" href="mailto:ums@ums.ac.id"><div class="d m1" style="border-style:none;position:absolute;left:383.411987px;bottom:765.078979px;width:60.994019px;height:10.924011px;background-color:rgba(255,255,255,0.000001);"></div></a></div><div class="pi" data-data='{"ctm":[1.000000,0.000000,0.000000,1.000000,0.000000,0.000000]}'></div></div>
</div>
<div class="loading-indicator">

</div>
</body>
</html>
