<?php
$this->load->view('template/head');
?>

<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<!-- Content Header (Page header) -->
<script>
    window.print();
</script>

<img src="<?php echo base_url('assets/img/logo-ums-baru.jpg'); ?>" height="45px" width="45px">
UNIVERSITAS MUHAMMADIYAH SURAKARTA<br>
Jl. A. Yani Tromol Pos 1 Pabelan Kartasura Telp.(0271) 719483 (Hunting)<br>
Fax.(0271) 715448 Surakarta-57102. <u>Http://www.ums.ac.id</u> Email : <u> ums@ums.ac.id</u>
<hr>
<table>
    <tr>
        <td>Nomor</td>
        <td>: 314/A4-II/BAA/XI/2017</td>
    </tr>
    <tr>
        <td>Lampiran</td>
        <td>: 1(satu) bendol</td>
    </tr>
    <td>Hal</td>
    <td>: Izin Pindah Studi</td>
    </tr>
    <tr>
        <td>Kepada Yth. Rektor</td>
    </tr>
    <tr>
        <td>UNIVERSITAS VETERAN SUKOHARJO</td>
    </tr>
    <tr>
        <td>di - SUKOHARJO</td>
    </tr>
</table>
<br>
<br>
<br>
<i>Assalamualaikum Warahmatullahi Wabarakatuhu</i>
<br>
<br>
Dengan hormat, bersama ini Pimpinan Muhammadiyah Surakarta menerangkan bahwa :<br><br>
<table>
    <tr>
        <td>Nama</td>
        <td>: Zaenal Mutakin</td>
    </tr>
    <tr>
        <td>NIM</td>
        <td>: J41009003</td>
    </tr>
    <tr>
        <td>Jurusan Progdi</td>
        <td>: Kesehatan Masyarakat S1</td>
    </tr>
    <tr>
        <td>Fakultas</td>
        <td>: FIK</td>
    </tr>
    <tr>
        <td>Tahun Akademik Masuk</td>
        <td>: 2017/2018</td>
    </tr>
    <tr>
        <td>Status Akreditasi</td>
        <td>: Terakreditasi Berdasarkan Surat Keputusan BAN-PT</td>
    </tr>
    <tr>
        <td></td>
        <td>: Departemen Pendidikan Nasional Republik Indonesia</td>
    </tr>
    <tr>
        <td></td>
        <td>Nomor</td>
        <td>: 403/SK/BAN-PT/Akred/S/X/2014</td>
    </tr>
    <tr>
        <td></td>
        <td>Tanggal</td>
        <td>: 24 Oktober 2014</td>
    </tr>
    <tr>
        <td></td>
        <td>Peringkat</td>
        <td>: B</td>
    </tr>
</table>
<br>
<br>
<br>
Adalah benar-benar mahasiswa Universitas Muhammadiyah Surakarta dengan Transkrip/Laporan Perkembangan
Studi dan salinan Kartu Tanda Mahasiswa sebagaimana terlampir, Selanjutnya , pada Semester Gasal 2017/2018
atas permohonan sendiri yang bersangkutan ingin pindah studi ke : UNIVERSITAS VETERAN SUKOHARJO
<BR><br>
Mohon perpinsahan mahasiswa tersebut dapat diproses sesuai ketentuan yang berlaku di instusi Bapak/Ibu.
<br><br>
Demikian surat prngantar pindah studi ini dibuat dengan sebenarnya dan untuk dapat dipergunakan sebagaimana mestinya.
<br><br>
<i>Wassalamualaikum Warahmatullahi Wabarakatuhu</i>
<br>
<br>
<br>
Surakarta, 16 Nopember 2017<br>
Kepala baa<br><br><br><br><br>
Drs. Agus Mulyanto<br>
<u>NIK : 776</u><br><br><br>
Tembusan :<br><br><br>
1. Rektor UMS sebagai Laporan<br>
2. YTh. Rektor UNIVET SUKOHARJO<br>
3. Yth. Ketuga Unit Informatika Keuangan<br>
4. Yth. Ketua Unit Informatika Technology (IT)<br>
5. Yth. Kabag. Pengajaran BAA<br>
6. Arsip<br><br><br><br><br>


<?php
$this->load->view('template/js');
?>

<!--tambahkan custom js disini-->

<!-- DataTables -->
<script src="<?php echo base_url('assets/custom/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/custom/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script>
<!-- http://www.bootstraptoggle.com/ -->
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

<?php
$this->load->view('template/foot');
?>
