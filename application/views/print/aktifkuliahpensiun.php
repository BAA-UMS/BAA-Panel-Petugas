<!DOCTYPE html>
<!-- Created by pdf2htmlEX (https://github.com/coolwanglu/pdf2htmlex) -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"/>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <link rel="stylesheet"
          href="<?php echo base_url('assets/css/surataktifkuliah/aktifkuliahpensiun/') ?>base.min.css"/>
    <link rel="stylesheet"
          href="<?php echo base_url('assets/css/surataktifkuliah/aktifkuliahpensiun/') ?>fancy.min.css"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/surataktifkuliah/aktifkuliahpensiun/') ?>main.css"/>
    <script src="<?php echo base_url('assets/js/surataktifkuliah/aktifkuliahpensiun/') ?>compatibility.min.js"></script>
    <script src="<?php echo base_url('assets/js/surataktifkuliah/aktifkuliahpensiun/') ?>theViewer.min.js"></script>
    <script>
        try {
            theViewer.defaultViewer = new theViewer.Viewer({});
        } catch (e) {
        }
    </script>
    <script>
        window.print();
    </script>
    <title>SK Aktif</title>
</head>
<body>
<div id="sidebar">
    <div id="outline">
    </div>
</div>
<div id="page-container">
    <div id="pf1" class="pf w0 h0" data-page-no="1"><div class="pc pc1 w0 h0"><img class="bi x0 y0 w1 h1" alt="" src="<?php echo base_url('assets/images/surataktifkuliah/aktifkuliahpensiun/') ?>bg1.png"/><div class="c x1 y1 w0 h2"><div class="t m0 x2 h3 y2 ff1 fs0 fc0 sc0 ls0 ws0">UNIVERSITAS MUHAMMADIYAH SURAKARTA</div><div class="t m0 x3 h3 y3 ff1 fs0 fc0 sc0 ls0 ws0">BIRO ADMINISTRASI AKADEMIK</div><div class="t m0 x4 h4 y4 ff2 fs1 fc1 sc0 ls0 ws0">           JL. A. Yani Tromol Pos 1 Pabelan Kartasura Telp. (0271) 717417,719483 ext 106 Fax. (0271) 715448 </div><div class="t m0 x5 h4 y5 ff2 fs1 fc1 sc0 ls0 ws0">Surakarta - 57102, http://www.ums.ac.id, Email: ums@ums.ac.id</div></div><div class="t m0 x3 h5 y6 ff2 fs2 fc1 sc0 ls0 ws0">LAMPIRAN : Surat Edaran Bersama Menteri</div><div class="t m0 x6 h5 y7 ff2 fs2 fc1 sc0 ls0 ws0">Keuangan Kepala Badan Administrasi Kepegawaian </div><div class="t m0 x6 h5 y8 ff2 fs2 fc1 sc0 ls0 ws0">Negara </div><div class="t m0 x7 h5 y9 ff2 fs2 fc1 sc0 ls0 ws0">Nomor : SE.1.38/DJA/I.C/7/80 (NO. SE/117/80)</div><div class="t m0 x7 h5 ya ff2 fs2 fc1 sc0 ls0 ws0">Nomor : 19/SE/1980</div><div class="t m0 x7 h5 yb ff2 fs2 fc1 sc0 ls0 ws0">Tanggal : 7 Juli 1980</div><div class="t m0 x8 h6 yc ff1 fs2 fc1 sc0 ls0 ws0">SURAT PERNYATAAN MASIH KULIAH/KURSUS/SEKOLAH</div><div class="t m0 x9 h5 yd ff2 fs2 fc1 sc0 ls0 ws0">XXXXXXXXXXXXXX</div><div class="t m0 xa h5 ye ff2 fs2 fc1 sc0 ls0 ws0">Yang bertandatangan di bawah ini :</div><div class="t m0 xa h5 yf ff2 fs2 fc1 sc0 ls0 ws0">1.<span class="_ _0"> </span>Nama<span class="_ _1"> </span>: Dr. Triyono, M.Si.</div><div class="t m0 xa h5 y10 ff2 fs2 fc1 sc0 ls0 ws0">2.<span class="_ _0"> </span>NIK<span class="_ _2"> </span>: 642</div><div class="t m0 xa h5 y11 ff2 fs2 fc1 sc0 ls0 ws0">3.<span class="_ _0"> </span>Pangkat, Golongan, Ruang<span class="_ _3"> </span>: IV/A-PEMBINA, Lektor Kepala</div><div class="t m0 xa h5 y12 ff2 fs2 fc1 sc0 ls0 ws0">4.<span class="_ _0"> </span>Jabatan<span class="_ _4"> </span>: Kepala BAA</div><div class="t m0 xa h5 y13 ff2 fs2 fc1 sc0 ls0 ws0">5.<span class="_ _0"> </span>Nama<span class="_ _1"> </span>: XXXXXXXX</div><div class="t m0 xa h5 y14 ff2 fs2 fc1 sc0 ls0 ws0">6.<span class="_ _0"> </span>NIM<span class="_ _5"> </span>: <?php echo $datamhs['nim'] ?></div><div class="t m0 xb h5 y15 ff2 fs2 fc1 sc0 ls0 ws0">adalah benar Mahasiswa<span class="_ _6"> </span>: Universitas Muhammadiyah Surakarta.</div><div class="t m0 xa h5 y16 ff2 fs2 fc1 sc0 ls0 ws0">7.<span class="_ _0"> </span>Program Studi<span class="_ _7"> </span>: XXXXXXXX</div><div class="t m0 xa h5 y17 ff2 fs2 fc1 sc0 ls0 ws0">8.<span class="_ _0"> </span>Jenjang Pendidikan<span class="_ _8"> </span>: XX</div><div class="t m0 xa h5 y18 ff2 fs2 fc1 sc0 ls0 ws0">9.<span class="_ _0"> </span>Fakultas<span class="_ _9"> </span>: XXXXXXXXXX</div><div class="t m0 xa h5 y19 ff2 fs2 fc1 sc0 ls0 ws0">10. Semester, Tahun Akademik<span class="_ _a"> </span>: XXXXXXX</div><div class="t m0 xb h5 y1a ff2 fs2 fc1 sc0 ls0 ws0">dan bahwa wali anak tersebut adalah;</div><div class="t m0 xa h5 y1b ff2 fs2 fc1 sc0 ls0 ws0">11. Nama<span class="_ _1"> </span>:  <?php echo $datamhs['pmhsaktif_nm_ortu']?></div><div class="t m0 xa h5 y1c ff2 fs2 fc1 sc0 ls0 ws0">12. Pensiun Bekas/Janda<span class="_ _b"> </span>: <?php echo $datamhs['pmhsaktif_pensiunan']?></div><div class="t m0 xb h5 y1d ff2 fs2 fc1 sc0 ls0 ws0">Nomor Rekening/Pensiun<span class="_ _c"> </span>: XXXXXX</div><div class="t m0 x0 h5 y1e ff2 fs2 fc1 sc0 ls0 ws0">Demikian Surat Pernyataan ini dibuat dengan sesungguhnya, untuk dipergnakan sebagaimana </div><div class="t m0 x0 h5 y1f ff2 fs2 fc1 sc0 ls0 ws0">mestinya.</div><div class="t m0 xc h5 y20 ff2 fs2 fc1 sc0 ls0 ws0">Surakarta, <?php echo $datamhs['created_at'] ?></div><div class="t m0 xc h5 y21 ff2 fs2 fc1 sc0 ls0 ws0">an. Kepala BAA,</div><div class="t m0 xc h5 y22 ff2 fs2 fc1 sc0 ls0 ws0">Kabag Pengajaran</div><div class="t m0 xc h6 y23 ff1 fs2 fc1 sc0 ls0 ws0">Drs. Agus Mulyanto</div><div class="t m0 xc h5 y24 ff2 fs2 fc1 sc0 ls0 ws0">NIK : 776</div><a class="l" href="http://www.ums.ac.id,"><div class="d m1" style="border-style:none;position:absolute;left:275.927002px;bottom:696.205994px;width:84.437988px;height:10.924011px;background-color:rgba(255,255,255,0.000001);"></div></a><a class="l" href="mailto:ums@ums.ac.id"><div class="d m1" style="border-style:none;position:absolute;left:390.442017px;bottom:696.205994px;width:60.993958px;height:10.924011px;background-color:rgba(255,255,255,0.000001);"></div></a></div><div class="pi" data-data='{"ctm":[1.000000,0.000000,0.000000,1.000000,0.000000,0.000000]}'></div></div>
</div>
<div class="loading-indicator">

</div>
</body>
</html>
