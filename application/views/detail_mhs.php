<?php
$this->load->view('template/head');
?>

<?php
$this->load->view('template/topbar');
$this->load->view('template/sidebar');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
        <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Profile Mahasiswa</h3>
                    <br><br>
                    <table>
                        <tr>
                            <td>Nama Mahasiswa</td>
                            <td>: Rosyid Fajar Nugraha</td>
                        </tr>
                        <tr>
                            <td>Nomor Induk Mahasiswa</td>
                            <td>: L200140162</td>
                        </tr>
                        <tr>
                            <td>Tempat Lahir</td>
                            <td>: SURAKARTA</td>
                        </tr>
                        <tr>
                            <td>Tanggal Lahir</td>
                            <td>: 9 Juli 1996</td>
                        </tr>
                        <tr>
                            <td>Fakultas</td>
                            <td>: Fakultas Komunikasi Informatika</td>
                        </tr>
                        <tr>
                            <td>Program Studi</td>
                            <td>: Informatika</td>
                        </tr>
                        <tr>
                            <td>Semester</td>
                            <td>: 20171</td>
                        </tr>
                        <tr>
                            <td>Tahun Akademik</td>
                            <td>: 2017</td>
                        </tr>
                        <tr>
                            <td>Alamat Mahasiswa</td>
                            <td>: SURAKARTA</td>
                        </tr>
                        <tr>
                            <td>Nomor HP</td>
                            <td>: 085645555222</td>
                        </tr>
                    </table>
                    <br>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">
                        <?php echo $datapilihan['kategori_nama'];
                        ?>
                        <br>
                        Surat Pernyataan Persetujuan Orang Tua<br>


                        <!-- + Options
                         Full texts
                        pmhsstatus_id  Ascending 1
                        nim
                        pmhsstatus_kategori_id
                        pmhsstatus_keterangan
                        pmhsstatus_scan_ortu
                        pmhsstatus_scan_ods
                        pmhsstatus_scan_ortu -->
                        <img src="<?php echo base_url('assets/img/' . $datapilihan['pmhsstatus_scan_ortu']); ?>"
                             width="90px" height="90px"/><br>
                        <?php if ($datapilihan['kategori_nama'] == 'Pindah Program Studi'): ?>
                            Surat Keterang One Day Service<br>
                            <img src="<?php echo base_url('assets/img/' . $datapilihan['pmhsstatus_scan_ods']); ?>"
                                 width="90px" height="90px"/><br>
                        <?php endif ?>

                </div>
            </div>
        </div>

    </div>
</section>
<section class="content">
    <!-- /.box-header -->
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-body">
                    <h3 class="box-title">History Pengajuan Layanan</h3>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>No</th>
                            <!-- <th>NIM</th> -->
                            <!-- <th>Nama</th> -->
                            <th>Keperluan</th>
                            <th>Keuangan</th>
                            <th>TU Prodi</th>
                            <th>pmhsstatus_perpus</th>
                            <th>Cetak</th>
                            <th>Aksi</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $r = 0;
                        foreach ($semuadatanim

                        as $data){
                        $r++; ?>
                        <tr>
                            <td><?php echo $r; ?></td>

                            <td><?php echo '<a href="' . base_url('index.php/perubahanstatus/show/') . '/' . $data['nim'] . '/' . $data['pmhsstatus_id'] . '">' . $data['kategori_nama'] . '</a>'; ?></td>
                            <?php if ($data['pmhsstatus_bak'] != 0) {
                                echo '<td align="center">
            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
            </td>';
                            } else {
                                echo '<td align="center"></td>';
                            }
                            if ($data['pmhsstatus_prodi'] != 0) {
                                echo '<td align="center"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></td>';
                            } else {
                                echo '<td align="center"></td>';
                            }
                            if ($data['pmhsstatus_perpus'] != 0) {
                                echo '<td align="center"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span></td>';
                            } else {
                                echo '<td align="center"></td>';
                            }
                            if ($data['pmhsstatus_prodi'] != 0 && $data['pmhsstatus_bak'] != 0 && $data['pmhsstatus_perpus'] != 0)
                            {
                            echo '<td>';
                            echo '<a href="' . base_url('perubahanstatus/cetak/') . '/' . $data['pmhsstatus_id'] . '"<button type="button" class="btn btn-danger"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></button></a>';
                            echo '</td>';

                            echo '<td>';
                            ?>

                            <span onclick="validasi_bak(this)" id="<?php echo $data['pmhsstatus_id'] ?>" value=''>
          <?php
          if ($data['pmhsstatus_baa'] == 1) {
              echo '<input type="checkbox" checked data-toggle="toggle" data-on="Validasi" data-off="Belum Validasi" data-onstyle="success" data-offstyle="danger" data-size="normal"></td>';
          } else {
              echo '<input type="checkbox" data-toggle="toggle" data-on="Validasi" data-off="Belum Validasi" data-onstyle="success" data-offstyle="danger" data-size="normal"></td>';
          }
          // echo '<input  type="checkbox" checked data-toggle="toggle" data-on="Validasi" data-off="Belum Validasi" data-onstyle="success" data-offstyle="danger" data-size="normal">';
          // echo '</td>';

          }
          else {
              echo '<td align="center"></td>';
              echo '<td align="center"></td>';
          }
          echo "</tr>";
          } ?>

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Keperluan</th>
                            <th>Keuangan</th>
                            <th>TU Prodi</th>
                            <th>pmhsstatus_perpus</th>
                            <th>Cetak</th>
                            <th>Aksi</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>


<?php
$this->load->view('template/js');
?>

<!--tambahkan custom js disini-->

<!-- DataTables -->
<script src="<?php echo base_url('assets/custom/bower_components/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/custom/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
<!-- http://www.bootstraptoggle.com/ -->
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
            'paging': true,
            'lengthChange': false,
            'searching': false,
            'ordering': true,
            'info': true,
            'autoWidth': false
        })
    })
</script>


<?php
$this->load->view('template/foot');
?>
