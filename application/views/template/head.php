<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>BAA Pengajuan</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?php echo base_url('assets/custom/AdminLTE-2.0.5/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet"
          type="text/css"/>
    <!-- Font Awesome Icons -->
    <link href="<?php echo base_url('assets/custom/font-awesome-4.3.0/css/font-awesome.min.css') ?>" rel="stylesheet"
          type="text/css"/>

    <!-- Theme style -->
    <link href="<?php echo base_url('assets/custom/AdminLTE-2.0.5/dist/css/AdminLTE.min.css') ?>" rel="stylesheet"
          type="text/css"/>
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link href="<?php echo base_url('assets/custom/AdminLTE-2.0.5/dist/css/skins/_all-skins.min.css') ?>"
          rel="stylesheet" type="text/css"/>
    <!-- DataTables -->
    <link rel="stylesheet"
          href="<?php echo base_url('assets/custom/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>">

    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
