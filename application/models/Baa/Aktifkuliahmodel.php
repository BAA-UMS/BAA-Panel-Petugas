<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aktifkuliahmodel extends CI_Model 
{

	public function ambilData()
	{
		$this->db->select("
			pmhsaktif_id, 
			nim,
			DATE_FORMAT(pengajuan_mhs_aktif.created_at, '%d %M %Y') as created_at,
			pmhsaktif_keperluan,
			pmhsaktif_cetak,
			pmhsaktif_ttd
			");
		$this->db->from('pengajuan_mhs_aktif');
        return $this->db->get()->result_array();
	}

	public function getSatu($id)
	{
		$this->db->select("
			pmhsaktif_id, 
			nim,
			DATE_FORMAT(pengajuan_mhs_aktif.created_at, '%d %M %Y') as created_at,
			pmhsaktif_keperluan,
			pmhsaktif_cetak,
			pmhsaktif_nik,
			pmhsaktif_pensiunan,
			pmhsaktif_ttd,
			pmhsaktif_nm_ortu,
			pmhsaktif_instansi,
			pmhsaktif_golongan_id,
			golongan_kode,
			golongan_nama

			");
		$this->db->from('pengajuan_mhs_aktif');
		$this->db->join('ref_golongan', 'ref_golongan.golongan_id = pengajuan_mhs_aktif.pmhsaktif_golongan_id', "Left");
		$this->db->where('pmhsaktif_id', $id);
        return $this->db->get()->result_array();
    }
    public function UpdateValidasi($id, $val)
    {

        $data = array('pmhsaktif_ttd' => $val);
        $this->db->where('pmhsaktif_id', strval($id));
        $this->db->update('pengajuan_mhs_aktif', $data);
        # code...
    }

}

/* End of file Aktifkuliah.php */
/* Location: ./application/models/Aktifkuliah.php */