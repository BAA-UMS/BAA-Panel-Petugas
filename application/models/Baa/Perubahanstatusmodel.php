<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perubahanstatusmodel extends CI_Model
{

    public function ambilData()
    {
        $this->db->select("
	    	pmhsstatus_id,nim,
	    	pmhsstatus_kategori_id,
	    	pmhsstatus_bak,
	    	pmhsstatus_perpus,
	    	pmhsstatus_baa,
	    	pmhsstatus_prodi,
	    	DATE_FORMAT(pengajuan_mhs_perubahanstatus.created_at, '%d/%m/%Y') as created_at,
	    	kategori_nama"
        );
        $this->db->from('pengajuan_mhs_perubahanstatus');
        $this->db->join('ref_kategori', 'ref_kategori.kategori_id = pengajuan_mhs_perubahanstatus.pmhsstatus_kategori_id', "Left");
        // $this->db->where('res_id', $res_id);
        return $this->db->get()->result_array();
    }

    public function UpdateValidasi($id, $val)
    {

        $data = array('pmhsstatus_baa' => $val);
        $this->db->where('pmhsstatus_id', strval($id));
        $this->db->update('pengajuan_mhs_perubahanstatus', $data);
        # code...
    }

    public function getSatu($id)
    {
        $this->db->select("
        	pmhsstatus_id,
        	nim,
        	pmhsstatus_kategori_id,
        	pmhsstatus_bak,
        	pmhsstatus_perpus,
        	pmhsstatus_baa,
        	pmhsstatus_prodi,
            DATE_FORMAT(pengajuan_mhs_perubahanstatus.created_at, '%d %M %Y') as created_at,
        	pmhsstatus_scan_ortu,
        	pmhsstatus_scan_ods,
        	kategori_nama");
        $this->db->from('pengajuan_mhs_perubahanstatus');
        $this->db->join('ref_kategori', 'ref_kategori.kategori_id = pengajuan_mhs_perubahanstatus.pmhsstatus_kategori_id', "Left");
        $this->db->where('pmhsstatus_id', $id);
        return $this->db->get()->result_array();

    }

    public function semuadatanim($nim)
    {
        $this->db->select("
        	pmhsstatus_id,
        	nim,pmhsstatus_kategori_id,
        	pmhsstatus_bak,
        	pmhsstatus_perpus,
        	pmhsstatus_baa,
        	pmhsstatus_prodi,
        	DATE_FORMAT(pengajuan_mhs_perubahanstatus.created_at, '%d/%m/%Y') as created_at,
        	kategori_nama");
        $this->db->from('pengajuan_mhs_perubahanstatus');
        $this->db->join('ref_kategori', 'ref_kategori.kategori_id = pengajuan_mhs_perubahanstatus.pmhsstatus_kategori_id', "Left");
        $this->db->where('nim', $nim);
        return $this->db->get()->result_array();

        # code...
    }

}

/* End of file Perubahanstatusmodel.php */
/* Location: ./application/models/Perubahanstatusmodel.php */