<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perpustakaanmodel extends CI_Model {

	public function ambilData()
	{
		    $this->db->select("
		    	pmhsstatus_id,
	    		nim,
	    		pmhsstatus_kategori_id,
	    		pmhsstatus_perpus,
	    		DATE_FORMAT(pengajuan_mhs_perubahanstatus.created_at, '%d/%m/%Y') as created_at,
		    	kategori_nama"
		    );
		    $this->db->from('pengajuan_mhs_perubahanstatus');   
		    $this->db->join('ref_kategori', 'ref_kategori.kategori_id = pengajuan_mhs_perubahanstatus.pmhsstatus_kategori_id',"Left");
		    return $this->db->get()->result_array();
	}

	public function UpdateValidasi($id,$val)
	{

		$data = array('pmhsstatus_perpus' => $val);
		$this->db->where('pmhsstatus_id', strval($id));
		$this->db->update('pengajuan_mhs_perubahanstatus', $data);
		# code...
	}
	public function getSatu($id)
	{
			$this->db->select("
				pmhsstatus_id, 
				nim, 
				pmhsstatus_kategori_id, 
				pmhsstatus_perpus, 
				DATE_FORMAT(pengajuan_mhs_perubahanstatus.created_at, '%d/%m/%Y') as created_at,
				kategori_nama"
			);
		    $this->db->from('pengajuan_mhs_perubahanstatus');   
		    $this->db->join('ref_kategori', 'ref_kategori.kategori_id = pengajuan_mhs_perubahanstatus.pmhsstatus_kategori_id',"Left");
		    $this->db->where('pmhsstatus_id', $id);
		    return $this->db->get()->result_array();	
		}

}

/* End of file Perpustakaanmodel.php */
/* Location: ./application/models/Perpustakaanmodel.php */