<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Keuanganmodel');

    }

    public function index()
    {

        $data['semuadata'] = $this->Keuanganmodel->ambilData();


        // // $data['semuadata'];
        // // die();
        // // $this->load->view('welcome_message');

        $this->load->view('petugas_keuangan', $data);
    }

    public function update_validasi($id)
    {
        $satuData = $this->Keuanganmodel->getSatu($id)[0];
        if ($satuData['pmhsstatus_bak'] == 0) {
            $this->Keuanganmodel->updateValidasi($id, '1');
        } elseif ($satuData['pmhsstatus_bak'] == 1) {
            $this->Keuanganmodel->updateValidasi($id, '0');
        }


        $result = [
            'success' => true,
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

}
