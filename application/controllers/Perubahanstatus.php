<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perubahanstatus extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Baa/perubahanstatusmodel');
    }

    public function index()
    {
        $data['semuadata'] = $this->perubahanstatusmodel->ambilData();
        // print_r($data['semuadata']);
        $this->load->view('baa_perubahanStatus', $data);
    }

    public function show($nim, $id)
    {
        // $data['semuadata'] = $this->Mmodel->ambildata();
        $datapilihan = $this->perubahanstatusmodel->getSatu($id);


        $semuadatanim = $this->perubahanstatusmodel->semuadatanim($nim);
        $data = array('datapilihan' => $datapilihan[0],
            'semuadatanim' => $semuadatanim,
        );
        // print_r($semuadatanim);
        $this->load->view('detail_mhs', $data);
    }

    public function cetak($id)
    {
        $datapilihan = $this->perubahanstatusmodel->getSatu($id);
        $data['datamhs'] = $datapilihan[0];
        $data['datadump'] = 'isikan sesuai database';

        if ($datapilihan[0]['pmhsstatus_kategori_id'] == 1) {
            // echo "cuti";
            $this->load->view('print/suratcutikuliah', $data);
        } elseif ($datapilihan[0]['pmhsstatus_kategori_id'] == 2) {
            echo "pindah program reguler to inter";
            // $this->load->view('print/suratpengundurandiri', $data);
        } elseif ($datapilihan[0]['pmhsstatus_kategori_id'] == 3) {
            // echo "pindah program studi";
            $this->load->view('print/suratpindahprogram', $data);
        } elseif ($datapilihan[0]['pmhsstatus_kategori_id'] == 4) {
            // echo "pindah ke perguruan tinggi lain";
            $this->load->view('print/suratpindahstudi', $data);
        } else {
            // echo 'mengundurkan diri';
            $this->load->view('print/suratpengundurandiri', $data);
        }
        // $this->load->view('print/cetakMengundurkan', $data);
    }

    public function update_validasi($id)
    {
        $satuData = $this->perubahanstatusmodel->getSatu($id)[0];
        if ($satuData['pmhsstatus_baa'] == 0) {
            $this->perubahanstatusmodel->updateValidasi($id, '1');
        } elseif ($satuData['pmhsstatus_baa'] == 1) {
            $this->perubahanstatusmodel->updateValidasi($id, '0');
        }


        $result = [
            'success' => true,
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

}
