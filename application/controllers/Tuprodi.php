<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tuprodi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Tuprodimodel');
    }

    public function index()
    {
        $data['semuadata'] = $this->Tuprodimodel->ambilData();
        $this->load->view('petugas_tuprodi', $data);
    }

    public function update_validasi($id)
    {
        $satuData = $this->Tuprodimodel->getSatu($id)[0];
        if ($satuData['pmhsstatus_prodi'] == 0) {
            $this->Tuprodimodel->updateValidasi($id, '1');
        } elseif ($satuData['pmhsstatus_prodi'] == 1) {
            $this->Tuprodimodel->updateValidasi($id, '0');
        }


        $result = [
            'success' => true,
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }
}
