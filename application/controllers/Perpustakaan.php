<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perpustakaan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Perpustakaanmodel');

    }

    public function index()
    {
        $data['semuadata'] = $this->Perpustakaanmodel->ambilData();
        // $data['semuadata'];
        // die();
        // $this->load->view('welcome_message');

        $this->load->view('petugas_perpustakaan', $data);
    }

    public function update_validasi($id)
    {
        $satuData = $this->Perpustakaanmodel->getSatu($id)[0];
        if ($satuData['pmhsstatus_perpus'] == 0) {
            $this->Perpustakaanmodel->updateValidasi($id, '1');
        } elseif ($satuData['pmhsstatus_perpus'] == 1) {
            $this->Perpustakaanmodel->updateValidasi($id, '0');
        }


        $result = [
            'success' => true,
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }
}
