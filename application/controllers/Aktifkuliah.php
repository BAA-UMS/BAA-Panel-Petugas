<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aktifkuliah extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // $this->load->model('Baa/Aktifkuliah');
        $this->load->model('Baa/Aktifkuliahmodel');


    }

    public function index()
    {
        $template_name = 'baa_mhsaktif';
        $data = array(
            'semuadata' => $this->Aktifkuliahmodel->ambilData(),
        );

        $this->load->view($template_name, $data);
    }

    public function update_validasi($id)
    {
        $satuData = $this->Aktifkuliahmodel->getSatu($id)[0];

        if ($satuData['pmhsaktif_ttd'] == 0) {
            $this->Aktifkuliahmodel->updateValidasi($id, '1');
        } elseif ($satuData['pmhsaktif_ttd'] == 1) {
            $this->Aktifkuliahmodel->updateValidasi($id, '0');
        }

        // print_r($satuData['pmhsaktif_ttd']);

        $result = [
            'success' => true,
        ];

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($result));
    }

    public function cetak($id)
    {
        $datapilihan = $this->Aktifkuliahmodel->getSatu($id);
        $data['datamhs'] = $datapilihan[0];
        $data['datadump'] = 'isikan sesuai database';

        if (isset($datapilihan[0]['pmhsaktif_nik'])) {
            // echo "aktif bekerja";            
            $this->load->view('print/aktifkuliahbekerja', $data);
        } elseif (isset($datapilihan[0]['pmhsaktif_pensiunan'])) {
            // echo "aktif pensiun";
            $this->load->view('print/aktifkuliahpensiun', $data);
        } else {
            // echo 'aktif';
            $this->load->view('print/aktifkuliahbiasa', $data);
        }
    }
}
